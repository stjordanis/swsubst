# swsubst

SUBST and JOIN


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## SWSUBST.LSM

<table>
<tr><td>title</td><td>swsubst</td></tr>
<tr><td>version</td><td>3.2a</td></tr>
<tr><td>entered&nbsp;date</td><td>2002-11-27</td></tr>
<tr><td>description</td><td>SUBST and JOIN</td></tr>
<tr><td>keywords</td><td>freedos, subst, join</td></tr>
<tr><td>author</td><td>Steffen.Kaiser@fh-rhein-sieg.de</td></tr>
<tr><td>maintained&nbsp;by</td><td>Steffen.Kaiser@fh-rhein-sieg.de</td></tr>
<tr><td>platforms</td><td>dos</td></tr>
<tr><td>copying&nbsp;policy</td><td>[GNU General Public License, Version 2](LICENSE)</td></tr>
<tr><td>wiki&nbsp;site</td><td>http://wiki.freedos.org/wiki/index.php/Subst</td></tr>
</table>
